import { NgModule } from '@angular/core';
import { EpisodesRoutingModule } from './episodes-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';


import { EpisodesComponent } from './episodes/episodes.component';
import { EpisodesService } from './services/episodes.service';

@NgModule({
  declarations: [
    EpisodesComponent
  ],
  imports: [
    EpisodesRoutingModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [
    EpisodesService
  ]
})
export class EpisodesModule { }