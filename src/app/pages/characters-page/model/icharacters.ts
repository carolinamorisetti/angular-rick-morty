export interface Icharacters {
    id: number;
    name: string;
    image: string;
}

export interface IcharactersResponse {
    info: {
        count: number;
        next: string;
        pages: number;
        prev: string;
    };
    results: Icharacters[];
}


