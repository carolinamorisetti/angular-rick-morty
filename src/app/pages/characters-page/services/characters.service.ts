import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';

const baseUrl = 'https://rickandmortyapi.com/api/';
const characterUrl = baseUrl + 'character';

@Injectable()
export class CharactersService {

  constructor(private http: HttpClient) { }

  getCharacters() {
    return this.http.get(characterUrl);
  }
}
