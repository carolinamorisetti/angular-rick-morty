
import { NgModule } from '@angular/core';
import { HistoryRoutingModule } from './history-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



import { HistoryComponent } from './history/history.component';
import { HistoryFormComponent } from './history/history-form/history-form.component';




@NgModule({
  declarations: [HistoryComponent, HistoryFormComponent],
  imports: [
    HistoryRoutingModule,
    ReactiveFormsModule

  ],
  exports:  [
    HistoryComponent
  ]
})
export class HistoryModule { }